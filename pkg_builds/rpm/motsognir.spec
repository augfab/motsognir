#
# spec file for package motsognir
#
# Copyright (c) 2008-2021 Mateusz Viste
#

Name: motsognir
Version: 1.0.13
Release: 1%{?dist}
Summary: a robust, reliable and easy to install gopher server
Url: http://motsognir.sourceforge.net/
Group: Productivity/Networking/Other
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
License: MIT
Source0: %{name}-%{version}.tar.xz

BuildRequires: gcc xz

%description
Motsognir is a robust, reliable and easy to install open-source gopher server for Unix-like systems.

The configuration is done via a single configuration file with reasonable defaults. Motsognir supports server-side CGI applications and PHP scripts. It is entirely written in ANSI C, without dependencies.

%prep
%setup

%build
make

%check

%install
make install DESTDIR=%{buildroot}

%files
%attr(755, root, root) %dir /usr/share/doc/motsognir
%attr(644, root, root) %doc /usr/share/doc/motsognir/changes.txt
%attr(644, root, root) %doc /usr/share/doc/motsognir/manual.pdf
%attr(644, root, root) %doc /usr/share/doc/motsognir/motsognir.conf
%attr(644, root, root) %doc /usr/share/doc/motsognir/motsognir-extmap.conf
%attr(644, root, root) %doc /usr/share/man/man8/motsognir.8.gz
%attr(644, root, root) %config /etc/motsognir.conf
%attr(755, root, root) /usr/sbin/motsognir
%attr(755, root, root) /etc/init.d/motsognir

%changelog
* Sat Sep 24 2013 Mateusz Viste <mateusz@localhost>
 - v1.0 released
